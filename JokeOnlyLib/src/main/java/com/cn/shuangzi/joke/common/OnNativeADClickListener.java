package com.cn.shuangzi.joke.common;

import com.cn.shuangzi.joke.bean.NativeADModel;

/**
 * Created by CN.
 */

public interface OnNativeADClickListener {
    void onNativeADClick(NativeADModel nativeADModel,int position);
}
