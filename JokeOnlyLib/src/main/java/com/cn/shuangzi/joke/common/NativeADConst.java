package com.cn.shuangzi.joke.common;

/**
 * Created by CN.
 */

public class NativeADConst {
    public static final int TYPE_DATA = 1000;
    public static final int TYPE_GDT = 1001;
    public static final int TYPE_TT = 1002;
    public static final int TYPE_NONE = 0;

    public static final String BASE_URL_JOKE_RANDOM = "http://v.juhe.cn/joke/randJoke.php";
    public static final String APPKEY_JOKE = "be089aaca3d02d06b094385a3394cab3";
//    public static final String APPKEY_JOKE = "39094c8b40b831b8e7b7a19a20654ed7";
    public static final String URI = "uri";
    public static final String ELEMENT_NAME = "element_name";
}
