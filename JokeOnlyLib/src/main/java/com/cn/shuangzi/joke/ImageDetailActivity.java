package com.cn.shuangzi.joke;

import android.annotation.TargetApi;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.transition.AutoTransition;
import android.view.View;
import android.view.Window;

import com.cn.shuangzi.joke.bean.RandomBean;
import com.cn.shuangzi.joke.common.NativeADConst;
import com.cn.shuangzi.joke.view.photodraweeview.OnPhotoTapListener;
import com.cn.shuangzi.joke.view.photodraweeview.PhotoDraweeView;
import com.cn.shuangzi.SZManager;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.imagepipeline.image.ImageInfo;

import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.SharedElementCallback;
import androidx.core.view.ViewCompat;


public class ImageDetailActivity extends AppCompatActivity implements ControllerListener<ImageInfo>,
        PhotoDraweeView.OnPhotoLoadListener, View.OnClickListener {
    private RandomBean.ResultBean resultBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initWindowTransitions();
        setContentView(R.layout.activity_image_detail);
//        Util.keepFullScreen(this);
        resultBean = (RandomBean.ResultBean) getIntent().getSerializableExtra(NativeADConst.URI);
        if (resultBean == null || resultBean.url == null || resultBean.url.trim().equals("")) {
            finish();
            return;
        }
        findViewById(R.id.imgLeft).setOnClickListener(this);
        PhotoDraweeView draweeView = findViewById(R.id.photo_drawee_view);
        ViewCompat.setTransitionName(draweeView, NativeADConst.ELEMENT_NAME);
        draweeView.setPhotoUri(Uri.parse(resultBean.url));
        draweeView.setOnPhotoLoadListener(this);
        draweeView.setOnPhotoTapListener(new OnPhotoTapListener() {
            @Override
            public void onPhotoTap(View view, float x, float y) {
                onBackPressed();
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void initWindowTransitions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            AutoTransition transition = new AutoTransition();
            getWindow().setSharedElementEnterTransition(transition);
            getWindow().setSharedElementExitTransition(transition);
            ActivityCompat.setEnterSharedElementCallback(this, new SharedElementCallback() {
                @Override
                public void onSharedElementEnd(List<String> sharedElementNames,
                                               List<View> sharedElements, List<View> sharedElementSnapshots) {
                    for (final View view : sharedElements) {
                        if (view instanceof PhotoDraweeView) {
                            ((PhotoDraweeView) view).setScale(1f, true);
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onSubmit(String id, Object callerContext) {

    }

    @Override
    public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
    }

    @Override
    public void onIntermediateImageSet(String id, ImageInfo imageInfo) {

    }

    @Override
    public void onIntermediateImageFailed(String id, Throwable throwable) {

    }

    @Override
    public void onFailure(String id, Throwable throwable) {

    }

    @Override
    public void onRelease(String id) {

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.imgLeft) {
            onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            SZManager.getInstance().onUMResume(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            SZManager.getInstance().onUMPause(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
