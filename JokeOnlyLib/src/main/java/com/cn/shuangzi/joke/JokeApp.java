package com.cn.shuangzi.joke;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

/**
 * Created by CN.
 */

public class JokeApp {
    public static Application application;
    public static void init(Application application){
        Fresco.initialize(application);
        JokeApp.application = application;
    }
}
