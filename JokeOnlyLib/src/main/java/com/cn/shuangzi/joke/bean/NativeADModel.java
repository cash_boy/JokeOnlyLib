package com.cn.shuangzi.joke.bean;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.cn.shuangzi.joke.common.NativeADConst;


/**
 * Created by CN.
 */

public class NativeADModel implements MultiItemEntity {
    private NativeADChannel nativeADChannel;

    @Override
    public int getItemType() {
        switch (nativeADChannel) {
            case GDT:
                return NativeADConst.TYPE_GDT;
            case TT:
                return NativeADConst.TYPE_TT;
        }
        return NativeADConst.TYPE_NONE;
    }

    public NativeADChannel getNativeADChannel() {
        return nativeADChannel;
    }

    public void setNativeADChannel(NativeADChannel nativeADChannel) {
        this.nativeADChannel = nativeADChannel;
    }
    public void loadGDTView(FrameLayout frameLayout){
    }
    public void loadTTView(ImageView imgView, Activity activity) {
    }

    public void renderTTView(ViewGroup viewShow, View viewClick) {
    }

    @Override
    public String toString() {
        return "NativeADModel{" +
                "nativeADChannel=" + nativeADChannel +
                '}';
    }
}
