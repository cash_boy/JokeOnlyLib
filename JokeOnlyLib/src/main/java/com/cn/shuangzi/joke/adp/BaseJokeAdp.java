package com.cn.shuangzi.joke.adp;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.cn.shuangzi.joke.ImageDetailActivity;
import com.cn.shuangzi.joke.bean.NativeADModel;
import com.cn.shuangzi.joke.bean.RandomBean;
import com.cn.shuangzi.joke.common.NativeADConst;
import com.cn.shuangzi.joke.common.OnNativeADClickListener;
import com.facebook.drawee.view.SimpleDraweeView;

import java.io.Serializable;
import java.util.List;

import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.app.SharedElementCallback;
import androidx.core.view.ViewCompat;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;

/**
 * Created by CN.
 */

public abstract class BaseJokeAdp extends BaseMultiItemQuickAdapter<MultiItemEntity, BaseViewHolder> {
    private OnNativeADClickListener onNativeADClickListener;
    protected Activity activity;
    private List<MultiItemEntity> multiItemEntityList;
    public BaseJokeAdp(final Activity activity, final List<MultiItemEntity> multiItemEntityList, OnNativeADClickListener onNativeADClickListener, final boolean isPicture) {
        super(multiItemEntityList);
        this.multiItemEntityList = multiItemEntityList;
        this.activity = activity;
        addItemTypes();
        this.onNativeADClickListener = onNativeADClickListener;
        setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (multiItemEntityList.get(position - 2) instanceof NativeADModel) {
                    if (BaseJokeAdp.this.onNativeADClickListener != null) {
                        BaseJokeAdp.this.onNativeADClickListener.onNativeADClick((NativeADModel) multiItemEntityList.get(position- 2), position- 2);
                    }
                } else {
                    if (isPicture) {
                        ViewCompat.setTransitionName(view, NativeADConst.ELEMENT_NAME);
                        ActivityOptionsCompat optionsCompat =
                                ActivityOptionsCompat.makeSceneTransitionAnimation(
                                        activity, view, NativeADConst.ELEMENT_NAME);
                        ActivityCompat.setExitSharedElementCallback(activity,
                                new SharedElementCallback() {
                                    @Override
                                    public void onSharedElementEnd(List<String> sharedElementNames,
                                                                   List<View> sharedElements, List<View>
                                                                           sharedElementSnapshots) {
                                        super.onSharedElementEnd(sharedElementNames, sharedElements,
                                                sharedElementSnapshots);
                                        for (final View view : sharedElements) {
                                            if (view instanceof SimpleDraweeView) {
                                                view.setVisibility(View.VISIBLE);
                                            }
                                        }
                                    }
                                });
                        Intent intent = new Intent(activity, ImageDetailActivity.class);
                        intent.putExtra(NativeADConst.URI, (Serializable) getData().get(position - 2));
                        ActivityCompat.startActivity(activity, intent, optionsCompat.toBundle());
                    }
                }
            }

        });
    }

    protected void loadGDTView(BaseViewHolder helper, MultiItemEntity item) {
    }

    protected void loadTTCommonView(BaseViewHolder helper, MultiItemEntity item) {
    }

    @Override
    public int getItemViewType(int position) {
        if (this.multiItemEntityList != null && this.multiItemEntityList.size() > 0) {
            MultiItemEntity multiItemEntity = this.multiItemEntityList.get(position);
            if (multiItemEntity instanceof NativeADModel) {
                NativeADModel nativeADModel = (NativeADModel) multiItemEntity;
                return nativeADModel.getItemType();
            } else if (multiItemEntity instanceof RandomBean.ResultBean) {
                return NativeADConst.TYPE_DATA;
            }
        }
        return super.getItemViewType(position);
    }

    public abstract void loadTTView(BaseViewHolder helper, MultiItemEntity item);

    public abstract void addItemTypes();
}
