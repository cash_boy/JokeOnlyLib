package com.cn.shuangzi.joke.view;

import android.content.Context;
import android.util.AttributeSet;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.cn.shuangzi.joke.adp.RandomPictureJokeAdp;
import com.cn.shuangzi.joke.bean.NativeADModel;
import com.cn.shuangzi.joke.common.NativeADConst;
import com.cn.shuangzi.joke.common.OnNativeADClickListener;
import com.cn.shuangzi.util.SZUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by CN.
 */

public class PictureJokeView extends BaseJokeView implements OnNativeADClickListener {
    private RandomPictureJokeAdp pictureJokeAdp;

    public PictureJokeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PictureJokeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public int getADImgWidth() {
        return getWidth() - SZUtil.dip2px(30);
    }

    @Override
    public int getADImgHeight() {
        return SZUtil.dip2px(150);
    }

    @Override
    public Map<String, String> getParams() {
        Map<String, String> map = new HashMap<>();
        map.put("key", NativeADConst.APPKEY_JOKE);
        map.put("type", "pic");
        return map;
    }

    @Override
    public String getRequestTag() {
        return "PictureJokeView";
    }

    @Override
    public void setAdapterData(List<MultiItemEntity> multiItemEntityList) {
        if(pictureJokeAdp == null){
            pictureJokeAdp = new RandomPictureJokeAdp(activity,multiItemEntityList,this);
            recyclerView.setIAdapter(pictureJokeAdp);
        }else{
            pictureJokeAdp.notifyDataSetChanged();
        }
    }

    @Override
    public void onNativeADClick(NativeADModel nativeADModel, int position) {

    }
}
