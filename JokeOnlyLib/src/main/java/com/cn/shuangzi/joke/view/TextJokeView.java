package com.cn.shuangzi.joke.view;

import android.content.Context;
import android.util.AttributeSet;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.cn.shuangzi.joke.adp.RandomTextJokeAdp;
import com.cn.shuangzi.joke.bean.NativeADModel;
import com.cn.shuangzi.joke.common.NativeADConst;
import com.cn.shuangzi.joke.common.OnNativeADClickListener;
import com.cn.shuangzi.util.SZUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by CN.
 */

public class TextJokeView extends BaseJokeView implements OnNativeADClickListener {
    private RandomTextJokeAdp textJokeAdp;
    public TextJokeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TextJokeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public int getADImgWidth() {
        return SZUtil.dip2px(110);
    }

    @Override
    public int getADImgHeight() {
        return SZUtil.dip2px(90);
    }

    @Override
    public Map<String, String> getParams() {
        Map<String, String> map = new HashMap<>();
        map.put("key", NativeADConst.APPKEY_JOKE);
        return map;
    }

    @Override
    public String getRequestTag() {
        return "TextJokeView";
    }

    @Override
    public void setAdapterData(List<MultiItemEntity> multiItemEntityList) {
        if(textJokeAdp == null){
            textJokeAdp = new RandomTextJokeAdp(activity,multiItemEntityList,this);
            recyclerView.setIAdapter(textJokeAdp);
        }else{
            textJokeAdp.notifyDataSetChanged();
        }
    }

    @Override
    public void onNativeADClick(NativeADModel nativeADModel, int position) {

    }
}
